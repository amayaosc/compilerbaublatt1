#include <stdlib.h>
#include <string.h>
#include <stdio.h>

const size_t INTSTACK_INIT_CAPACITY = 32;

void die(char* msg) {
  fprintf(stderr, "%s\n", msg);
  exit(EXIT_FAILURE);
}

typedef struct {
  size_t size;
  size_t capacity;
  int* data;
} IntStack;

int stackInit(IntStack *self) {
  self->size = 0;
  self->capacity = INTSTACK_INIT_CAPACITY;
  self->data = malloc(self->capacity * sizeof(int));

  return self->data ? 0 : -1;
}

// NOTE: Caller frees IntStack*, since in test there are automatic
//       storage duration non malloc'ed unfreeable IntStacks 
void stackRelease(IntStack *self) {
  if (self)
    free(self->data);
}

void stackResize(IntStack *self, size_t new_capacity) {
  self->capacity = new_capacity;
  self->data = realloc(self->data, self->capacity * sizeof(int));
}

void stackPush(IntStack *self, int i) {
  self->size += 1;

  if (self->size >= self->capacity)
    stackResize(self, 2 * self->capacity);

  self->data[self->size - 1] = i;
}

int stackTop(const IntStack *self) {
  if (!self->size) {
    stackRelease((IntStack *) self);
    die("err: stackTop() called on empty stack");
  }

  return self->data[self->size - 1];
}

int stackPop(IntStack *self) {
  if (!self->size) {
    stackRelease(self);
    die("err: stackPop() called on empty stack");
  }

  self->size -= 1;

  if (4 * self->size <= self->capacity)
    stackResize(self, self->capacity >> 1);
 
  return self->data[self->size];
}

int stackIsEmpty(const IntStack *self) {
  return self->size == 0;
}

void stackPrint(const IntStack *self) {
  printf("top(");

  if (self->size) {
    for (size_t i = self->size - 1; i > 0; i--)
      printf("%d, ", self->data[i]);

    printf("%d", self->data[0]);
  }

  printf(")bot\n");
}
