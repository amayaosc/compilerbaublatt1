// syntaxbaum nach firstChild/rightSibling struktur.
// die kindknoten von self sind also:
//      self->firstChild,
//      self->firstChild->rightSibling,
//      self->firstChild->rightSibling->rightSibling, ...
// ownership of a Syntree is not assumed, since pointers given to release()
//      in test were not malloc'ed
//      and therefore were unfreeable automatic storage duration variables
//      therefore heap allocated syntrees have to be freed seperately by the owner/caller

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include "syntree.h"

int syntreeInit(Syntree *self) {
  if (!self)
    return -1;

  self->root = 0;

  return 0;
}

void syntreeReleaseNode(SyntreeNodeID self) {
  if (!self)
    return;

  for (SyntreeNodeID it = self->firstChild; it;) {
    SyntreeNodeID del = it;
    it = it->rightSibling;
    syntreeReleaseNode(del);
  }

  free(self);
}

void syntreeRelease(Syntree *self) {
  if (self)
    syntreeReleaseNode(self->root);
}

SyntreeNodeID syntreeNodeNumber(Syntree *self, int number) {
  SyntreeNodeID new = malloc(sizeof(SyntreeNode));

  new->val = number;
  new->firstChild = 0;
  new->rightSibling = 0;

  return new;
}

SyntreeNodeID syntreeNodeTag(Syntree *self, SyntreeNodeID id) {
  SyntreeNodeID new = malloc(sizeof(SyntreeNode));

  new->firstChild = id;
  new->rightSibling = 0;

  self->root = new;

  return new;
}

SyntreeNodeID syntreeNodePair(Syntree *self, SyntreeNodeID id1, SyntreeNodeID id2) {
  SyntreeNodeID new = malloc(sizeof(SyntreeNode));

  new->firstChild = id1;
  new->rightSibling = 0;

  id1->rightSibling = id2;
  self->root = new;

  return new;
}

SyntreeNodeID syntreeNodeAppend(Syntree *self, SyntreeNodeID list, SyntreeNodeID elem) {
  SyntreeNodeID it;
  for (it = list->firstChild; it && it->rightSibling; it = it->rightSibling) {} // no op
  
  if (it)
    it->rightSibling = elem;
  else
    list->firstChild = elem;

  return list;
}

SyntreeNodeID syntreeNodePrepend(Syntree *self, SyntreeNodeID elem, SyntreeNodeID list) {
  elem->rightSibling = list->firstChild;
  list->firstChild = elem;

  return list;
}

void indent(size_t indentation) {
  for (size_t i = 0; i < indentation; i++)
    printf("  ");
}

void syntreePrintLeveled(SyntreeNodeID root, size_t level) {
  if (!root)
    return;
  
  if (root->firstChild) {
    indent(level);
    printf("{\n");

    for (SyntreeNodeID it = root->firstChild; it; it = it->rightSibling)
      syntreePrintLeveled(it, level + 1);

    indent(level);
    printf("}\n");
  } else {
    indent(level);
    printf("(%d)\n", root->val);
  }
}

void syntreePrint(const Syntree *self, SyntreeNodeID root) {
  syntreePrintLeveled(root, 0);
}
